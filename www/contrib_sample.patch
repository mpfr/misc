diff --git a/doc/algorithms.tex b/doc/algorithms.tex
index b37417c57..1012eb397 100644
--- a/doc/algorithms.tex
+++ b/doc/algorithms.tex
@@ -4609,6 +4609,19 @@ independently by Mark Dickinson:
 \end{center}
 The smallest bound is $e=186564318007$ for $b=7$ and $b=49$.
 
+\subsection{The \texttt{add17} function}
+
+This special function is defined mathematically by
+$\mathrm{add17}(x) = x+17$. We compute it by simply calling
+\texttt{mpfr\_add\_ui} with arguments $x$ and $17$
+(which is representable in an \texttt{unsigned long}):
+\[ y \leftarrow \circ(x + 17). \]
+Since there is only one atomic operation in the algorithm, there is no
+intermediate rounding error, and correct rounding is provided by
+\texttt{mpfr\_add\_ui}.
+Special cases, exceptions and in-place operations
+are dealt by \texttt{mpfr\_add\_ui}.
+
 \subsection{Summary}
 
 Table ~\ref{table:genericError} presents the generic error for several operations, assuming all variables have a
diff --git a/doc/mpfr.texi b/doc/mpfr.texi
index 2b8cedc6a..7e9a6e7a6 100644
--- a/doc/mpfr.texi
+++ b/doc/mpfr.texi
@@ -2131,6 +2131,11 @@ when @var{rop} and @var{op1} are identical.
 Set @var{rop} to the factorial of @var{op}, rounded in the direction @var{rnd}.
 @end deftypefun
 
+@deftypefun int mpfr_add17 (mpfr_t @var{rop}, const mpfr_t @var{op}, mpfr_rnd_t @var{rnd})
+Set @var{rop} to the value of @var{op} plus 17, rounded in the direction
+@var{rnd}.
+@end deftypefun
+
 @deftypefun int mpfr_fma (mpfr_t @var{rop}, const mpfr_t @var{op1}, const mpfr_t @var{op2}, const mpfr_t @var{op3}, mpfr_rnd_t @var{rnd})
 @deftypefunx int mpfr_fms (mpfr_t @var{rop}, const mpfr_t @var{op1}, const mpfr_t @var{op2}, const mpfr_t @var{op3}, mpfr_rnd_t @var{rnd})
 Set @var{rop} to @tm{(@var{op1} @GMPtimes{} @var{op2}) + @var{op3}}
diff --git a/src/Makefile.am b/src/Makefile.am
index 1f1ea4279..77ea44d10 100644
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -73,7 +73,7 @@ random_deviate.h random_deviate.c erandom.c mpfr-mini-gmp.c             \
 mpfr-mini-gmp.h fmma.c log_ui.c gamma_inc.c ubf.c invert_limb.h         \
 invsqrt_limb.h beta.c odd_p.c get_q.c pool.c total_order.c set_d128.c   \
 get_d128.c nbits_ulong.c cmpabs_ui.c sinu.c cosu.c tanu.c fmod_ui.c     \
-acosu.c asinu.c atanu.c compound.c exp2m1.c exp10m1.c powr.c
+acosu.c asinu.c atanu.c compound.c exp2m1.c exp10m1.c powr.c add17.c
 
 nodist_libmpfr_la_SOURCES = $(BUILT_SOURCES)
 
diff --git a/src/add17.c b/src/add17.c
new file mode 100644
index 000000000..9f283ed6b
--- /dev/null
+++ b/src/add17.c
@@ -0,0 +1,29 @@
+/* mpfr_add17 -- add a floating-point number with the magic constant 17
+
+Copyright 2024 Free Software Foundation, Inc.
+Contributed by the AriC and Caramba projects, INRIA.
+
+This file is part of the GNU MPFR Library.
+
+The GNU MPFR Library is free software; you can redistribute it and/or modify
+it under the terms of the GNU Lesser General Public License as published by
+the Free Software Foundation; either version 3 of the License, or (at your
+option) any later version.
+
+The GNU MPFR Library is distributed in the hope that it will be useful, but
+WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
+or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
+License for more details.
+
+You should have received a copy of the GNU Lesser General Public License
+along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
+https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
+51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */
+
+#include "mpfr-impl.h"
+
+int
+mpfr_add17 (mpfr_ptr a, mpfr_srcptr b, mpfr_rnd_t rnd_mode)
+{
+  return mpfr_add_ui (a, b, 17, rnd_mode);
+}
diff --git a/src/mpfr.h b/src/mpfr.h
index f5c63641b..e11a9f281 100644
--- a/src/mpfr.h
+++ b/src/mpfr.h
@@ -660,6 +660,7 @@ __MPFR_DECLSPEC int mpfr_exp2m1 (mpfr_ptr, mpfr_srcptr, mpfr_rnd_t);
 __MPFR_DECLSPEC int mpfr_exp10m1 (mpfr_ptr, mpfr_srcptr, mpfr_rnd_t);
 __MPFR_DECLSPEC int mpfr_eint (mpfr_ptr, mpfr_srcptr, mpfr_rnd_t);
 __MPFR_DECLSPEC int mpfr_li2 (mpfr_ptr, mpfr_srcptr, mpfr_rnd_t);
+__MPFR_DECLSPEC int mpfr_add17 (mpfr_ptr, mpfr_srcptr, mpfr_rnd_t);
 
 __MPFR_DECLSPEC int mpfr_cmp  (mpfr_srcptr, mpfr_srcptr);
 __MPFR_DECLSPEC int mpfr_cmp3 (mpfr_srcptr, mpfr_srcptr, int);
diff --git a/tests/Makefile.am b/tests/Makefile.am
index e3bc75009..2afc57dfe 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -75,7 +75,7 @@ TESTS_NO_TVERSION = tabort_prec_max tassert tabort_defalloc1            \
      tsinh tsinh_cosh tsinu tsprintf tsqr tsqrt tsqrt_ui tstckintc      \
      tstdint tstrtofr tsub tsub1sp tsub_d tsub_ui tsubnormal tsum tswap \
      ttan ttanh ttanu ttotal_order ttrunc tui_div tui_pow tui_sub       \
-     turandom tvalist ty0 ty1 tyn tzeta tzeta_ui
+     turandom tvalist ty0 ty1 tyn tzeta tzeta_ui tadd17
 
 check_PROGRAMS = tversion $(TESTS_NO_TVERSION)
 
diff --git a/tests/reuse.c b/tests/reuse.c
index 9070c614c..6c566352c 100644
--- a/tests/reuse.c
+++ b/tests/reuse.c
@@ -689,6 +689,7 @@ main (void)
           test2 (mpfr_zeta, "mpfr_zeta", p, (mpfr_rnd_t) rnd);
           test2 (mpfr_gamma, "mpfr_gamma", p, (mpfr_rnd_t) rnd);
           test2 (mpfr_lngamma, "mpfr_lngamma", p, (mpfr_rnd_t) rnd);
+          test2 (mpfr_add17, "mpfr_add17", p, (mpfr_rnd_t) rnd);
 
           test2 (mpfr_rint, "mpfr_rint", p, (mpfr_rnd_t) rnd);
           test2 (mpfr_rint_ceil, "mpfr_rint_ceil", p, (mpfr_rnd_t) rnd);
diff --git a/tests/tadd17.c b/tests/tadd17.c
new file mode 100644
index 000000000..b30df9d9b
--- /dev/null
+++ b/tests/tadd17.c
@@ -0,0 +1,39 @@
+/* Test file for mpfr_add17
+
+Copyright 2024 Free Software Foundation, Inc.
+Contributed by the AriC and Caramba projects, INRIA.
+
+This file is part of the GNU MPFR Library.
+
+The GNU MPFR Library is free software; you can redistribute it and/or modify
+it under the terms of the GNU Lesser General Public License as published by
+the Free Software Foundation; either version 3 of the License, or (at your
+option) any later version.
+
+The GNU MPFR Library is distributed in the hope that it will be useful, but
+WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
+or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
+License for more details.
+
+You should have received a copy of the GNU Lesser General Public License
+along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
+https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
+51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */
+
+#include "mpfr-test.h"
+
+#define TEST_FUNCTION mpfr_add17
+#define TEST_RANDOM_EMIN -10
+#define TEST_RANDOM_EMAX 10
+#include "tgeneric.c"
+
+int
+main (int argc, char *argv[])
+{
+  tests_start_mpfr ();
+
+  test_generic (MPFR_PREC_MIN, 100, 100);
+
+  tests_end_mpfr ();
+  return 0;
+}
