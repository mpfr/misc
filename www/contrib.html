<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>How To Contribute to GNU MPFR?</title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" href="styles/screen.css" media="screen,tv,projection" />
<link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
<link rel="top" title="The GNU MPFR Library" href="index.html" />
<link rel="up" title="The GNU MPFR Library" href="index.html" />
</head>

<body>

<div class="logo"><a href="index.html"><img src="mpfr100.png" height="40" width="100" alt="[MPFR]" /></a></div>

<h1>How To Contribute to <cite><acronym>GNU</acronym> <acronym>MPFR</acronym></cite>?</h1>

<p>There are several ways to contribute to
<cite><acronym>MPFR</acronym></cite>:</p>
<ul>
<li>contribute a new mathematical function (see
<a href="#new-fct">below</a>);</li>
<li>find, report and fix bugs;</li>
<li>improve the code coverage and/or contribute new test cases;</li>
<li>improve the documentation;</li>
<li>measure and improve the efficiency of the code.</li>
</ul>

<p>The <a href="mpfr-current/mpfr.html#Reporting-Bugs"><cite>Reporting Bugs</cite></a> section of the <acronym>MPFR</acronym> manual gives details on how to report new bugs.</p>

<h2 id="new-fct">How To Contribute a New Mathematical Function to
<cite><acronym>MPFR</acronym></cite>?</h2>

<p>Assume you want to write the code, say <code>mpfr_foo</code>,
for a new mathematical function (say <var>foo</var>):</p>

<ul>

<li>You (or your employer, depending on your status or your country)
first have to agree to donate your contribution to the
<cite><acronym>MPFR</acronym></cite> project, more precisely to agree with
the license used by <cite><acronym>MPFR</acronym></cite>, to agree to assign
the copyright of your code to the
<acronym title="Free Software Foundation">FSF</acronym>
(<a href="https://www.gnu.org/licenses/why-assign.html">this page</a>
explains why). In addition, if it applies, your employer has to agree to
disclaim rights on your contribution.</li>

<li>You need to define precisely the function you want to implement, in
mathematical terms. For example, the <em>dilogarithm</em> function has
different meanings in the literature. Use either a power series, an integral
representation... If possible, give references to the literature.</li>

<li>Then you need to choose an algorithm to evaluate the function
<var>foo</var>. Write this algorithm, by detailing each atomic operation.
Then you have to perform an error analysis of your algorithm (both the
mathematical error, <abbr>i.e.</abbr>, when neglecting terms of a Taylor
series, and the roundoff error). See for example the
<a href="algo.html"><samp>algorithms.tex</samp></a> document
(<a href="algorithms.pdf"><acronym>PDF</acronym> version</a>), which
contains numerous examples of such algorithms and error analyses. In some
cases, you will need different algorithms depending on the output precision
and/or on the input range. Please write this description in LaTeX, so that
it can easily be included in the <samp>algorithms.tex</samp> document.</li>

<li>Implement in C the algorithm(s) described in the previous step.
You have to follow the following guidelines:
   <ul>
   <li>conform to the <acronym>ISO</acronym> C90 and C99 standards;</li>
   <li>conform to the
       <a href="https://www.gnu.org/prep/standards/"><acronym>GNU</acronym>
       Coding Standards</a>;</li>
   <li>do not use machine floating-point types (<code>float</code>,
       <code>double</code>, <abbr>etc.</abbr>);</li>
   <li>do not use functions defined in <samp>&lt;math.h&gt;</samp>, since
       they might give different results on different configurations;</li>
   <li>further technical information is available in the
       <a href="https://gitlab.inria.fr/mpfr/mpfr/-/blob/master/doc/README.dev?ref_type=heads"><samp>README.dev</samp></a> file from the
       <cite><acronym>MPFR</acronym></cite> <cite>Git</cite>
       repository.</li>
   </ul>
For example, use the following to decide if the computed approximation
enables one to deduce correct rounding:
<pre>    MPFR_CAN_ROUND (approx, approx_err, out_prec, rnd_mode)</pre></li>

<li>Implement a test program (say <samp>tfoo.c</samp>) that tests your
implementation for correctness. This test program should:
   <ul>
   <li>test special values (�0, �Inf, NaN);</li>
   <li>test exact values, for example <code>exp2</code>(1)�=�2,
       <code>sqrt</code>(25) = 5, <abbr>etc.</abbr>;</li>
   <li>test hard-coded values that you have computed with another software
       tool, using a large precision. Please input those values in the same
       format as in the <samp>tests/data/exp</samp> file for example. Please
       indicate if those input/output pair were formally verified. See the
       <code>data_check</code> function in <samp>tests/tests.c</samp>;</li>
   <li>test random values: generate a random number <var>x</var>,
       compute <code>foo(x)</code> with say <var>p</var> bits, then
       <code>foo(x)</code> with <var>p</var>+10 bits, and check that
       both results are compatible. A useful function that does that
       automatically is <code>test_generic</code> from
       <samp>tests/tgeneric.c</samp>;</li>
   <li>test bad cases for the correct rounding. If the inverse function
       has been implemented, the <code>bad_cases</code> function from
       <samp>tests/tests.c</samp> can be used;</li>
   <li>test in-place operations, <abbr>i.e.</abbr>, with same input and
       output pointers, like <code>mpfr_foo (a, a, ...)</code>. See the
       <samp>reuse.c</samp> test program.</li>
   </ul>
</li>

<li>Write the documentation of the <code>mpfr_foo</code> function in the
file <samp>mpfr.texi</samp>.</li>

<li>(Optional) Test the efficiency of your implementation and compare it
to other software tools.</li>

<li>Send your contribution as a patch (obtained with <code>git diff</code>) with
respect to the development version to the <cite><acronym>MPFR</acronym></cite>
developers. They will review your contribution to decide if it will be
included in <cite><acronym>MPFR</acronym></cite>. In that case, your
contribution will be acknowledged in the corresponding source files and
in the documentation.</li>

<li>Here is an <a href="contrib_sample.patch">example of such a patch</a>,
which contributes the <code>mpfr_add17</code> function to
<cite><acronym>MPFR</acronym></cite> (note: this patch predates the
source reorganization, so that some files should actually be in the
<samp>doc</samp> or <samp>src</samp> directories; moreover, the new
<samp>add17.c</samp> and <samp>tadd17.c</samp> files are not provided
in this example).</li>

</ul>

<p><a href="index.html">Back to the <cite><acronym>MPFR</acronym></cite>
page.</a></p>

<hr />

<div class="footer"><a href="https://validator.w3.org/check/referer"><img
  src="https://www.w3.org/Icons/valid-xhtml10"
  alt="Valid XHTML 1.0!" height="31" width="88" /></a></div>

</body>

</html>
