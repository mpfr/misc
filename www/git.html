<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>MPFR - Git Repository</title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" href="styles/screen.css" media="screen,tv,projection" />
<link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
<link rel="top" title="The GNU MPFR Library" href="index.html" />
<link rel="up" title="The GNU MPFR Library" href="index.html" />
</head>

<body>

<div class="logo"><a href="index.html"><img src="mpfr100.png" height="40" width="100" alt="[MPFR]" /></a></div>

<h1><cite><acronym>MPFR</acronym></cite> - <cite>Git</cite> Repository</h1>

<p>The <cite><acronym>MPFR</acronym></cite> repository is hosted on <a href="https://gitlab.inria.fr/mpfr/mpfr" title="MPFR repository on Inria GitLab">Inria GitLab</a>.</p>

<h2>Basic Usage</h2>

<p>You can get the latest development version by using the anonymous <a href="https://git-scm.com/"><cite>Git</cite></a> access. To clone the repository, you can use the following command:</p>

<blockquote><p><code>git clone https://gitlab.inria.fr/mpfr/mpfr.git</code></p></blockquote>

<p>If you have an Inria GitLab account, you can alternatively clone the repository with <acronym>SSH</acronym> by using the following command:</p>

<blockquote><p><code>git clone git@gitlab.inria.fr:mpfr/mpfr.git</code></p></blockquote>

<p>These commands will create a directory named <samp>mpfr</samp> with a local <cite>Git</cite> repository and the latest version of the source (<samp>master</samp> branch). <strong>Please, read the <samp>doc/README.dev</samp> file</strong> (<a href="https://gitlab.inria.fr/mpfr/mpfr/-/raw/master/doc/README.dev" title="Online version of the doc/README.dev file">online version</a>). In particular, the <cite>Git</cite> tree does not have a <samp>configure</samp> script; you need some development tools to generate it (more information is given in the <samp>README.dev</samp> file).</p>

<p>This allows you to benefit from the correction of bugs and various other improvements since the latest release. But as usual with development code, it may temporarily be unstable or have bugs preventing from compiling it (please report any problem so that it is fixed as soon as possible). You should use the <a href="mpfr-current/">latest <cite><acronym>MPFR</acronym></cite> release</a> with the published patches if you do not want such problems.</p>

<h2>Branches and Tags</h2>

<p>From a <cite>Git</cite> working tree, a branch <code><var>branchname</var></code> can be checked out with the following command:</p>

<blockquote><p><code>git checkout <var>branchname</var></code></p></blockquote>

<p>(More precisely, if the local branch does not exist yet, this will create one and use the corresponding remote branch for tracking.)</p>

<p>Similarly, a tag <code><var>tagname</var></code> can be checked out with the following command:</p>

<blockquote><p><code>git checkout <var>tagname</var></code></p></blockquote>

<p><cite><acronym>MPFR</acronym></cite> releases are based on branches whose name has the form <code><var>major</var>.<var>minor</var></code>, where <code><var>major</var></code> and <code><var>minor</var></code> are two integers. There may be other kinds of branches, such as feature branches. The list of branches (including all these remote branches) can be obtained with the following command:</p>

<blockquote><p><code>git branch -a</code></p></blockquote>

<p>For each release branch, a tag of the form <code><var>major</var>.<var>minor</var>-root</code> is associated with the commit on which the branch is based (just before the first commit in this branch). A tag of the form <code><var>major</var>.<var>minor</var>.<var>patchlevel</var></code> is associated with the commit on which such a release is based. The list of all tags can be obtained with the following command:</p>

<blockquote><p><code>git tag -l</code></p></blockquote>

<p>You can share a same local <cite>Git</cite> repository between multiple working trees, each tracking a different branch. For instance, from a working tree, you can create a new one <code>../mpfr-4.1</code> for the 4.1 branch with the following command:</p>

<blockquote><p><code>git worktree add ../mpfr-4.1 4.1</code></p></blockquote>

<p>The <a href="https://stackoverflow.com/questions/15954631/git-changes-to-branch-since-created">changes to a branch since it was created</a> can be obtained with the following command (for the checked out branch, <code><var>branchname</var></code> may be omitted), with a triple dot:</p>

<blockquote><p><code>git diff master...<var>branchname</var></code></p></blockquote>

<p>For the release branches, the <code><var>major</var>.<var>minor</var>-root</code> tags can also be used, e.g.:</p>

<blockquote><p><code>git diff <var>branchname</var>-root..<var>branchname</var></code></p></blockquote>

<h2>History</h2>

<p>The first repository for <cite><acronym>MPFR</acronym></cite> was created on 1999-06-09, using <a href="https://en.wikipedia.org/wiki/Concurrent_Versions_System"><cite><acronym>CVS</acronym></cite></a>. This repository was converted to <a href="https://subversion.apache.org/"><cite>Subversion</cite></a> on 2005-10-24. Then this <cite>Subversion</cite> repository was converted to <cite>Git</cite> on 2021-09-29, with an upload to Inria GitLab on 2021-10-15.</p>

<p>Due to some particularities of the <cite>Subversion</cite> repository and incorrect commit history dating back from <cite><acronym>CVS</acronym></cite> (due to bugs in the <cite>cvs2svn</cite> converter), the conversion from <cite>Subversion</cite> to <cite>Git</cite> had to be done with Eric S. Raymond's <a href="http://www.catb.org/~esr/reposurgeon/"><cite>reposurgeon</cite></a> tool to fix everything (but also to add some metadata). The configuration files and scripts used for this conversion can be found in the <a href="mpfr-conversion.tar.xz"><samp>mpfr-conversion.tar.xz</samp></a> archive.</p>

<h2>Documentation</h2>

<ul>
<li><a href="https://git-scm.com/doc"><cite>Git</cite></a>.</li>
<li><a href="https://gcc.gnu.org/git.html">Documentation written for <acronym title="GNU Compiler Collection">GCC</acronym> developers</a> (this contains additional general information about <cite>Git</cite>).</li>
</ul>

<p><a href="index.html">Back to the <cite><acronym>MPFR</acronym></cite>
page.</a></p>

<hr />

<div class="footer"><a href="https://validator.w3.org/check/referer"><img
  src="https://www.w3.org/Icons/valid-xhtml10"
  alt="Valid XHTML 1.0!" height="31" width="88" /></a></div>

</body>

</html>
