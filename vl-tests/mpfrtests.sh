#!/bin/sh

# Test of MPFR tarballs with various options.
#
# Written in 2011-2024 by Vincent Lefevre <vincent@vinc17.net>.
# This script is free software; you can copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# Usage: ./mpfrtests.sh [ +<host> ] [ <archive.tar.*> ] < mpfrtests.data
#    or  ./mpfrtests.sh -C
#
# In normal use (first form):
#   A +<host> argument can be needed when the FQDN is not set correctly.
#   The default <archive.tar.*> value is "mpfr-tests.tar.gz".
#   A file mpfrtests.<host>.out is output.
# With the -C option (second form):
#   Clean-up: Any temporary mpfrtests directory is removed (such
#   a directory isn't removed automatically in case of abnormal
#   termination, in order to be able to debug).
#
# Environment variables MAKE (default: "make") and MAKE_JOBS (an integer)
# can be provided either via mpfrtests.data (preferred for MAKE) or via
# the command line (preferred for MAKE_JOBS).
#
# Use the MPFR_TESTS="t1 t2" environment variable to test only t1 and t2.

# For the Solaris /bin/sh
{ a=a; : ${a%b}; } 2> /dev/null || test ! -x /usr/xpg4/bin/sh || \
  exec /usr/xpg4/bin/sh -- "$0" ${1+"$@"}

# No longer use /tmp for the temporary test directory. 2 reasons:
#   * On multi-user machines, this may not be safe; e.g. with libtoolize:
#       https://debbugs.gnu.org/cgi/bugreport.cgi?bug=21951
#       https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=805454
#   * /tmp may be mounted with the noexec option.
#prefix="${TMPDIR:-/tmp}/mpfrtests-$USER-"
prefix="$HOME/mpfrtests-$(uname -n)-"

if [ "x$1" = "x-C" ]; then
  chmod -R u+w "$prefix"*
  rm -rf "$prefix"*
  exit 0
fi

set -e

# Avoid errors due to missing locales (e.g. from ICC).
export LC_ALL=C

# Additional tests.
# Test MPFR_FOO can be disabled for a particular "make check"
# with "ENV:MPFR_FOO" (which unsets MPFR_FOO for this run).
export MPFR_CHECK_LIBC_PRINTF=1
export MPFR_CHECK_MAX=1
export MPFR_CHECK_BADCASES=1
export MPFR_SUSPICIOUS_OVERFLOW=1

case "$1" in
  +*) fqdn=${1#+}; shift ;;
  *)  fqdn=$(perl -MPOSIX -e 'print((gethostbyname((uname)[1]))[0])') ;;
esac
printf "Host: %s\n" "$fqdn"

pwd=$(pwd)
out=$pwd/mpfrtests.$fqdn.out
rm -f "$out"

dd="------------------------------------------------------------------------"
ed="========================================================================"
gmprx='.*gmp.h version and libgmp version are the same... '
mj='${MAKE:-make} ${MAKE_JOBS:+-j$MAKE_JOBS}'
tgz="${1:-mpfr-tests.tar.gz}"
cr="$(printf \\r)"
os="$(uname)"

case "$tgz" in
  *bz2) unarch=bunzip2 ;;
  *gz)  unarch=gunzip ;;
  *xz)  unarch=unxz ;;
  *) echo "Unknown file extension in $tgz" >&2; exit 1 ;;
esac

# Note: the "|| true" is for NetBSD's buggy sh.
unset tmpdir || true

# We need a grep that supports the -E and -q options (POSIX).
# Under Solaris, we can use ggrep from /opt/csw/bin, which is in the path.
# In case things still do not work, see AC_PROG_EGREP from Autoconf.
grep=grep
case "$os" in
  SunOS) $grep -E -q . /dev/null 2> /dev/null || grep=ggrep ;;
esac

# We need something that should work in all POSIX shells...
dotee()
{
  rm -f "$2.st"
  { eval "$1"; echo $? > "$2.st"; } 2>&1 | tee "$2"
  st=$(cat "$2.st" 2> /dev/null)
  [ -n "$st" ]
  rm -f "$2.st"
  [ "$st" -eq 0 ] || exit "$st"
}

lineout()
{
  printf "%s %s (%d) $dd\n" "---" "$fqdn" $ntests | cut -b -56 >> "$out"
}

fileinfo()
{
  # Digest::SHA has been in Perl since 5.9.3 (released on 2006-01-28).
  # perl -MModule::CoreList -e \
  #   'print Module::CoreList->first_release("Digest::SHA"),"\n";'
  perl -0e "
    use strict;
    use Digest::SHA;
    my \$s = <>;
    my \$v =
      \$s =~ /^timestamp=[\"']?(\\d{4}-\\d\\d-\\d\\d)[\"']?$/m ||
      \$s =~ /^VERSION=[\"']?(.*?)[\"']?$/m ||
      \$s =~ /^# (serial .*)$/m ? \" (\$1)\" : '';
    my \$sha = Digest::SHA->new('SHA-1');
    \$sha->add(\$s);
    my \$hash = substr(\$sha->hexdigest,0,8);
    printf \"  %-13s %s%s\\n\", '$1', \$hash, \$v;
    " "$srcdir/$1" >> "$out"
}

tst()
{
  srcdir=${1:-.}
  first=1
  while read line
  do
    printf "%s\n" "$line"
    case "$line" in
      PROC:*)
        [ -n "$first" ]
        echo "* $fqdn ($($srcdir/config.guess) / ${line#PROC:})" > "$out"
        [ -z "$1" ] || echo "with objdir != srcdir" >> "$out"
        uname -a >> "$out"
        unset os || true
        if [ -f /etc/os-release ]; then
          os1=$(. /etc/os-release; echo "$PRETTY_NAME")
        elif [ -f /etc/release ]; then
          os1=$(head -n 1 /etc/release)
        fi
        # http://stackoverflow.com/a/3352015/3782797
        os2="${os1#"${os1%%[![:space:]]*}"}"
        os3="${os2%"${os2##*[![:space:]]}"}"
        [ -z "$os3" ] || printf "OS: %s\n" "$os3" >> "$out"
        version=$(cat $srcdir/VERSION)
        echo "MPFR version: $version${xvers:+ [$xvers]}" >> "$out"
        versnum=$(eval "expr $(echo $version | \
          sed -n 's/^\([0-9]\{1,\}\)\.\([0-9]\{1,\}\)\.\([0-9]\{1,\}\).*/10000 \\* \1 + 100 \\* \2 + \3/p')")
        # For config.guess and config.sub, the following information can be
        # useful to compare with the public files.
        # For configure and ltmain.sh, which are generated locally, this is
        # useful mainly to compare with other tarballs or working trees.
        # If need be, one could add information on the Makefile.in files.
        # Among the m4 libtool files, m4/libtool.m4 is the most interesting
        # one, so let's consider it here. Note that these files are used to
        # generate the configure script, but are not used for the build; so
        # their version information is not much useful, and the hash of the
        # configure script can be used to detect changes somewhere.
        fileinfo config.guess
        fileinfo config.sub
        fileinfo configure
        fileinfo ltmain.sh
        fileinfo m4/libtool.m4
        ;;
      CHECK-BEGIN*)
        [ -z "$first" ]
        [ -z "$check" ]
        if expr $versnum ${line#CHECK-BEGIN} > /dev/null; then
          check=1
          lineout
          if [ -z "$1" ]; then
            conf="./configure"
          else
            mkdir obj
            cd obj
            conf="../$1/configure"
          fi
        else
          while read line
          do
            if [ "$line" = CHECK-END ]; then break; fi
          done
        fi
        env=""
        ;;
      ENV:*)
        [ -n "$check" ]
        # NOTE: The environment will be restored before the next test.
        v=${line#ENV:}
        var=${v%%=*}
        val=$(env | $grep "^$var=" || true)
        if [ -z "$val" ]; then
          env="$env unset $var;"
        else
          env="$env export $var=\"${val#*=}\";"
        fi
        printf "ENV: %s\n" "$v" >> "$out"
        # Unset the variable for the case "ENV:VAR"; this is different
        # from setting it to null. Since the variable may reference to
        # itself, it would be incorrect to do an unconditional "unset"
        # followed by the "export" (for the PATH variable, this would
        # also be incorrect).
        if [ "$v" = "$var" ]; then
          eval "unset $var || true"
        else
          eval "export $v"
        fi
        ;;
      EVAL:*)
        [ -z "$first" ]
        # NOTE: Do not use commands that could affect later tests unless
        # this is done on purpose, such as setting environment variables
        # (for this, use the "ENV:" feature). Use "EVAL:" to run commands
        # that affect the build, not to output information ("INFO:").
        cmd=${line#EVAL:}
        printf "\$ %s\n" "$cmd" >> "$out"
        eval $cmd
        ;;
      CONF:*)
        [ -n "$check" ]
        [ -n "$conf" ]
        #
        # The following workaround was for GCC bug 86554 / 87276, which
        # has now been fixed.
        #if [ "$line" = "CONF:CC=gcc-snapshot" ]; then
        #  line="$line -fno-code-hoisting"
        #fi
        #
        # Quote each configure parameter with double quotes, thus allowing
        # expansion of environment variables (possibly set with "ENV:").
        conf="$conf \"${line#CONF:}\""
        ;;
      INFO:*)
        [ -n "$check" ]
        if [ -n "$conf" ]; then
          echo '$ ./config.status -V' >> "$out"
          echo "*** Running configure ***"
          echo "$conf"
          dotee "$conf" mpfrtests.cfgout
          ./config.status -V | sed '/with options/q' >> "$out"
          $grep '^mpfr_cv_' config.log >> "$out"
          $grep -E '^(CC|CFLAGS|DEFS)=' config.log >> "$out"
          gmpv1=$(sed -n "s/$gmprx//p" mpfrtests.cfgout)
          if [ -z "$gmpv1" ]; then
            # Mini-gmp does not define a GMP version.
            if ! $grep -q '^DEFS=.*-DMPFR_USE_MINI_GMP=1' config.log; then
              echo "$0: can't get GMP version" >&2
              exit 1
            fi
          elif [ "x$gmpv1" != "xcannot test" ]; then
            gmpvers=$(printf "%s\n" "$gmpv1" | \
                      sed -n "s/^(\(.*\)\/.*) yes/\1/p")
            if [ -z "$gmpvers" ]; then
              # A mismatch at configure time could be seen on
              # FreeBSD 13.0-RELEASE-p3 (gcc303.fsffrance.org):
              #   [...] are the same... (6.1.2/6.2.1) no
              # but "make check" is correct:
              #   GMP: header 6.1.2, library 6.1.2
              # Let's use the version of the GMP header.
              gmpvers=$(printf "%s\n" "$gmpv1" | \
                        sed -n "s/^(\(.*\)\/.*) no/\1 (?)/p")
              if [ -z "$gmpvers" ]; then
                echo "$0: internal error with GMP version" >&2
                exit 1
              fi
            fi
            printf "[GMP %s]\n" "$gmpvers" >> "$out"
          fi
          rm mpfrtests.cfgout
          CC=$(sed -n 's/^CC *= *//p' Makefile)
          unset conf || true
        fi
        cmd=${line#INFO:}
        case "$cmd" in
          CCV:*)
            printf "CC = %s\n" "$CC" >> "$out"
            eval "$CC ${cmd#CCV:} | head -n 1" >> "$out" 2> /dev/null
            ;;
          *)
            printf "\$ %s\n" "${cmd% | head -n 1}" >> "$out"
            eval $cmd >> "$out"
            ;;
        esac
        ;;
      CHECK-END)
        [ -n "$check" ]
        [ -z "$conf" ]
        env | $grep '^GMP_CHECK_RANDOMIZE=' >> "$out" || true
        echo "*** Running make ***"
        dotee "$mj" mpfrtests.makeout
        rm mpfrtests.makeout
        echo "*** Running make check ***"
        make_check="$mj check${MPFR_TESTS:+ TESTS='tversion $MPFR_TESTS'}"
        if [ "$os" = SunOS ]; then
          # Workaround to the following libtool bug concerning Solaris:
          #   https://debbugs.gnu.org/cgi/bugreport.cgi?bug=30222
          #   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=888059
          make_check="LD_LIBRARY_PATH=\$PWD/src/.libs $make_check"
        fi
        dotee "$make_check" mpfrtests.makeout
        # For tests with MinGW/Wine, the first tversion line is sometimes
        # missing while the tversion test did not fail, which is a bug in
        # MinGW/Wine. Bug report:
        #   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=914822
        # Detect this now and exit with an error message when this occurs.
        if ! $grep -q '^\[tversion] MPFR [1-9]' mpfrtests.makeout; then
          echo "$0: missing first tversion line" >&2
          exit 1
        fi
        # Note: We need to remove the possible CR characters, as produced
        # by checks under Wine. But the sed from Solaris does not support
        # \r or even [:cntrl:]; indeed, neither of the following works:
        #   printf "a\rb\n" | sed -n "s/\r//p"
        #   printf "a\rb\n" | sed -n "s/[[:cntrl:]]//p"
        # while GNU sed 4.2.2 is fine even with the --posix option.
        # But we can do:
        #   printf "a\rb\n" | sed -n "s/$(printf \\r)//p"
        sed -n "/Testsuite summary/,\$ {
                  s/^\(\[tversion\].*[^$cr]\)$cr*/\1/p
                  / PASS:/,/ FAIL:/H
                  / ERROR:/ {
                    g
                    s!.* PASS: *\([0-9]\{1,\}\).* SKIP: *\([0-9]\{1,\}\).* FAIL: *\([0-9]\{1,\}\).*!--> PASS: \1 / SKIP: \2 / FAIL: \3!p
                  }
                }" mpfrtests.makeout >> "$out"
        # Version for Automake before 1.13 (obsolete, disabled):
        true || \
          sed -n "/: tzeta_ui/,/tests passed/ {
                    s/^\(\[tversion\].*[^$cr]\)$cr*/\1/p
                    s/^\(.*tests passed\)/--> \1/p
                  }" mpfrtests.makeout >> "$out"
        if $grep -q '^\[tversion].*TLS = yes' mpfrtests.makeout; then
          # The configure script sets LC_ALL and LANGUAGE to C in order to
          # avoid translated strings, and to get compiler information, it
          # tries various compiler options (such as --version and -v). So
          # we can retrieve the thread model from the config.log file, but
          # due to the different compiler options that are tried, we may
          # get duplicate lines (e.g. with clang).
          $grep '^Thread model:' config.log | uniq >> "$out"
        fi
        echo "*** Running make check-gmp-symbols ***"
        if ! $grep -q check-gmp-symbols: Makefile; then
          echo "Feature not present. Nothing to do."
        elif $grep -q "GMP internals = yes" mpfrtests.makeout; then
          echo "GMP internals have been requested. Test disabled."
        else
          dotee "$mj check-gmp-symbols" mpfrtests.makeout
          echo "Checked that no internal GMP symbols are used." >> "$out"
        fi
        echo "*** Running make check-exported-symbols ***"
        if ! $grep -q check-exported-symbols: Makefile; then
          echo "Feature not present. Nothing to do."
        else
          dotee "$mj check-exported-symbols" mpfrtests.makeout
          echo "Checked that no symbols with a GMP reserved prefix are defined." >> "$out"
        fi
        echo "*** Cleaning up ***"
        if [ -z "$1" ]; then
          rm mpfrtests.makeout
          ${MAKE:-make} distclean
        else
          cd ..
          rm -rf obj
        fi
        [ -z "$env" ] || eval "$env"
        unset check || true
        ntests=$((ntests+1))
        ;;
      \#*)
        continue
        ;;
      ?*)
        echo "$0: bad data ($line)" >&2
        [ -d "$tmpdir" ] && { cd; chmod -R u+w "$tmpdir"; rm -rf "$tmpdir"; }
        exit 1
        ;;
      *)
        return 0
        ;;
    esac
    unset first || true
  done
}

tstall()
{
  printf "Tests in %s\n" "$(pwd)"
  while read line
  do
    [ "HOST:$fqdn" = "$line" ] && tst "$@"
  done
  return 0
}

mktmpdir()
{
  tmpdir="${prefix}$$"
  rm -rf "$tmpdir"
  mkdir "$tmpdir"
  chmod 700 "$tmpdir"
  [ ! -h "$tmpdir" ]
}

ntests=0
xvers=""

if [ -x configure ]; then
  # Here the temporary directory is not used by the script itself;
  # it is created in case it is used by mpfrtests.data commands.
  mktmpdir
  [ ! -f Makefile ] || ${MAKE:-make} distclean
  if [ -d .svn ]; then
    xvers=r$(svnversion)
    [ "$xvers" != "rUnversioned directory" ] || xvers=""
  elif [ -f version-ext.sh ] && \
       [ "x`git rev-parse --is-inside-work-tree 2> /dev/null`" = xtrue ]; then
    xvers=$(GREP=$grep sh version-ext.sh)
  fi
  tstall
  rm -rf "$tmpdir"
elif [ -f "$tgz" ]; then
  mktmpdir
  cd "$tmpdir"
  case "$tgz" in
    /*) ;;
    *) tgz="$pwd/$tgz" ;;
  esac
  $unarch -c "$tgz" | tar xf -
  # The timestamp of the most recent Makefile.in or configure file will
  # be output. This allows one to differentiate tarballs with the same
  # VERSION file and make sure that the right one is tested, supporting
  # complete or partial regeneration by autoconf/automake.
  # A particular reason is that during the release process, if testing
  # failed, one needs to fix the issue and generate a new tarball.
  # Ideally, we should take the most recent file, but tarballs may have
  # additional files (e.g., additional data for the tests) that one may
  # not want to take into account.
  xvers=$(find mpfr-* -name Makefile.in -o -name configure | perl -MPOSIX -e '
    @a = sort map { chomp; strftime("%Y-%m-%dT%T",gmtime((stat($_))[9])) } <>;
    print "$a[-1]\n"')
  # When $$ (the PID) is even, test MPFR with objdir != srcdir.
  case $$ in
    *[02468])
      # Make srcdir read-only to detect any attempt to modify it.
      chmod -R a-w mpfr-*
      tstall mpfr-*
      chmod -R u+w mpfr-*
      ;;
    *[13579])
      cd mpfr-*
      tstall
      ;;
    *)
      echo "$0: internal error" >&2
      exit 1
      ;;
  esac
  cd "$pwd"
  rm -rf "$tmpdir"
else
  echo "$0: can't find executable configure or .tar.gz file" >&2
  exit 1
fi

lineout
printf "\n$ed\n" >> "$out"

printf "OK, output in %s\n" "$out"
exit 0

# $Id: mpfrtests.sh 174185 2024-12-02 15:17:34Z vinc17/cventin $
